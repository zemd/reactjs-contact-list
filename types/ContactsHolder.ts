import type {RandomUserEntry} from "./RandomUserEntry";

export interface ContactsHolder {
    isLoading: boolean;
    contacts: RandomUserEntry[];
    search(searchTerm: string): void;
    getContact(id: number): RandomUserEntry | undefined;
}
