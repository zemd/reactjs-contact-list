import type {ContactsHolder} from "../../../../types/ContactsHolder";
import {useEffect, useRef, useState, useTransition} from "react";
import type {RandomUserEntry} from "../../../../types/RandomUserEntry";
import Fuse from 'fuse.js';
import {faker} from "@faker-js/faker";
import {RandomApiResponse} from "../../../../types/RandomApiResponse";

//
// this hook provides contacts state management functionality that
// should be shared with different components. In more advance project some
// library like recoiljs or mobx would be used.
//
export default function useContactsProvider(): ContactsHolder {
    const allContacts = useRef<RandomUserEntry[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [contacts, setContacts] = useState<RandomUserEntry[]>([]);
    const [,startTransition] = useTransition();

    useEffect(() => {
        const controller = new AbortController();
        const signal = controller.signal;

        setLoading(true);
        fetch(`https://randomuser.me/api/?page=1&results=50&seed=abc`, {signal})
            .then((res) => {
                return res.json();
            })
            .then((json: RandomApiResponse) => {
                const data = json.results.map((entry, ind: number) => {
                    entry._id = ind;
                    entry.position = faker.name.jobTitle();
                    return entry;
                });
                allContacts.current = data;
                setContacts(data.slice());
            })
            .catch((error) => {
                if (error.name === 'AbortError') {
                    // ignore
                } else {
                    console.error(error)
                }
            })
            .finally(() => {
                setLoading(false);
            });

        return () => {
            return controller.abort();
        }
    }, []);

    const search = async (searchTerm: string) => {
        try {
            if (!searchTerm) {
                setContacts(() => [...allContacts.current]);
                return;
            }
            // ideally it should be separate request to the server,
            // but since we assume that the data that came from the first fetch is enough,
            // I use fuzzy search to filter out the contact list
            const fuse = new Fuse(allContacts.current, {
                includeScore: true,
                keys: [{
                    name: 'name.first',
                }, {
                    name: 'name.last'
                }, {
                    name: 'phone'
                }, {
                    name: 'email'
                }]
            });

            startTransition(() => {
                const results = fuse
                    .search(searchTerm);

                setContacts(() => {
                    return results
                        .filter((item) => {
                            return item && parseFloat(`${item.score}`) < 0.1;
                        })
                        .map((searchItem) => searchItem.item);
                });
            });
        } catch (error) {
            console.error(error);
        }
    }

    const getContact = (contactId: number): RandomUserEntry | undefined => {
        return allContacts.current.find((item) => {
            return item._id === contactId
        });
    }

    return {
        contacts: contacts,
        isLoading: loading,
        search,
        getContact
    };
}