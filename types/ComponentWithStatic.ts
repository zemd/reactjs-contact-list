import type {ReactElement} from "react";

export type ComponentWithStatic<
    Props extends Record<string, any>,
    Static extends Record<string, any>
> = ((props: Props) => ReactElement) & Static