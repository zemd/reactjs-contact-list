import {createContext} from "react";
import {ContactsHolder} from "../../../../types/ContactsHolder";

const ContactsContext = createContext<ContactsHolder>({isLoading: false} as ContactsHolder);
ContactsContext.displayName = 'ContactsContext';

export default ContactsContext;