import {RandomUserEntry} from "./RandomUserEntry";

export type RandomApiResponse<T = RandomUserEntry> = {
    results: T[];
    info: {
        seed: string;
        results: number;
        page: number;
        version: string;
    };
}