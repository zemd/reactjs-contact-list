import {useContext} from "react";
import ContactsContext from "./ContactsContext";
import {ContactsHolder} from "../../../../types/ContactsHolder";

export default function useContacts() {
    const contacts = useContext<ContactsHolder>(ContactsContext);
    return contacts;
}